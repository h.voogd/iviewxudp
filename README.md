# iViewXudp

A python file that communicates with the iViewX recording software for SMI REDn (RED 500) or the SMI HiSpeed Tower.

It uses UDP sockets to send messages to the iViewX recording software, as is described in the protocols in the iViewX manual. 

Not all commands that are available are implemented. Most commonly used commands are implemented.
Keep in mind that all messages must end with b'\x0a\x0d\x00' (and in _that_ order), otherwise the iViewX recording software won't recognize the commands.

**Usage:**

import iViewXudp  #import the library

iViewXudp.debuglogging = True  #if you want to see what message is sent. 

iViewXudp.connect(iViewXudp.UDP_IP, iViewXudp.UDP_PORT)  #To connect to the server

**Recording:**
In the beginning of the experiment:

iViewXudp.startRecording()  #starts the recording

At the end of the experiment:

iViewXudp.stopRecording()  #Stops the recording.

iViewXudp.saveToFile(b'filename')  #saves the recording to a file. On the SMI recording computer! Give it a good filename that ends with the extension '.idf' Note the small 'b' before the filename. It is needed because iViewX needs bytestrings.


If you want the recording to be paused:

iViewXudp.pauseRecording()

To continue the recording:

iViewXudp.continueRecording()

Note that pauseRecording and continueRecording must be matched. A recording that is already paused cannot be paused. A recording that is already continued, cannot be continued. Also, a paused recording cannot be stopped, if a recording is paused and it needs to be stopped, it must first be continued before it can be stopped.

**In trials:**

At the start of a trial:

iViewXudp.newTrial()  #  Increases the trialnumber in BeGaze (SMIs analysis program).

At the start of a stimulus:

iViewXudp.remark(b'screenshotname.bmp')  #adds a remark. Note the small 'b' before the filename. It is needed because iViewX needs bytestrings.

iViewXudp.bitmap(b'screenshotname.bmp')  #adds a stimulus bitmap in BeGaze. Note the small 'b' before the filename. It is needed because iViewX needs bytestrings.

In your script, you want to make a screenshot here, and save it as b'screenshotname.bmp'. Of course useful names should be used here. You can also put your screenshots in a folder on the iViewX recorder computer and specify in the iViewX program where your screenshots are stored. If iViewX can find your screenshots, it will change the scene image to show your screenshot. That way, it is possible to have the eyegaze shown in the scene image on the actual image that the participant is looking at. The screenshots are also needed when analyzing the data in BeGaze.


It is good practice to both use the remark and the bitmap message with the same parameter to make sure that this stimulus is analyzed in BeGaze.

