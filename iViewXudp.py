"""
@author: Hubert Voogd, hubert.voogd@ru.nl
"""

import socket

debuglogging = False

UDP_IP = b"192.168.1.1"
UDP_PORT = 4444
MESSAGE = b"ET_STP " + b'\x0a\x0d\x00'  #  +chr(10) + chr(13) + chr(0)


sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

def setDebugLogging(b):
    global debuglogging
    debuglogging = b


def connect(ip = UDP_IP, port = UDP_PORT):
    global UDP_IP, UDP_PORT, sock, debuglogging
    UDP_IP = ip
    UDP_PORT = port
    if debuglogging:
        print("IP: ", UDP_IP, "Port: ", UDP_PORT)


def startRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_REC " + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def stopRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_STP " + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def pauseRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_PSE "  + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def continueRecording():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_CNT "  + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def newTrial():
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_INC "  + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def remark(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_REM " + s  + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def bitmap(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_BMP " + s + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)


def saveToFile(s):
    global UDP_IP, UDP_PORT, sock, debuglogging
    msg = b"ET_SAV " + s  + b'\x0a\x0d\x00'  #  + chr(10) + chr(13) + chr(0)
    sock.sendto(msg, (UDP_IP, UDP_PORT))
    if debuglogging:
        print(msg)

